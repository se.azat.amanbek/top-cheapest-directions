FROM composer:1.6.5
ADD ./ /project/
WORKDIR /project/
RUN composer global require hirak/prestissimo  && \
    composer install --ignore-platform-reqs --no-scripts && \
    composer dump-autoload --no-scripts

FROM php:7.2-apache-stretch
ADD --chown=www-data:www-data ./ /var/www/html
COPY --chown=www-data:www-data --from=0 /project/vendor /var/www/html/vendor
COPY --chown=www-data:www-data .env.example /var/www/html/.env
RUN rm /etc/apache2/ports.conf && \
    pecl bundle -d /usr/src/php/ext redis && docker-php-ext-install redis && rm -R -f /usr/src/php/ext/redis && \
    ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load
COPY docker_data/php.ini /usr/local/etc/php/
COPY docker_data/apache2.conf /etc/apache2/
COPY docker_data/sites-enabled/* /etc/apache2/sites-enabled/
