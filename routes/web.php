<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('main');
Route::get('/select-date', 'DirectionController@index')->name('directions.index');
Route::post('/directions', 'DirectionController@showCheapestTicketOnGivenDate')->name('directions.show');
