@extends('layouts.app')

@section('content')
    <div style="padding-top: 20px">
        <table border="1px">
            <tr>
                <td style="padding: 10px">Направление</td>
                <td style="padding: 10px;">Цена</td>
            </tr>
            <tr>
                <td style="padding: 10px">{{ $direction }}</td>
                <td style="padding: 10px">{{ isset($directionData->price) ? $directionData->price->amount . ' ' . $directionData->price->currency : '' }}</td>
            </tr>
        </table>
    </div>
@endsection
