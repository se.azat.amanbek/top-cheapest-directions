@extends('layouts.app')

@section('content')
    <div style="padding-top: 20px" tabindex="-1">
        <form action="{{ route('directions.show') }}" method="POST">
            {{ csrf_field() }}
            <p>Выберите дату: </p>
            <input type="date" name="date">
            @if ($errors->has('date'))
                <div class="alert alert-danger">
                    <p> {{ $errors->first('date') }}</p>
                </div>
            @endif
            <p style="padding-top: 20px">Выберите направление:</p>
            <select name="direction" id="directions">
                @foreach($popularDirections as $popularDirection)
                    <option value="{{ $popularDirection }}">{{ $popularDirection }}</option>
                @endforeach
            </select>
            @if ($errors->has('direction'))
                <div class="alert alert-danger">
                    <p> {{ $errors->first('direction') }}</p>
                </div>
            @endif
            <button>Найти</button>
        </form>
    </div>
@endsection
