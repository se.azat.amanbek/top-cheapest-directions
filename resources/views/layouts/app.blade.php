<!doctype html>
<html lang="en">
    @include('layouts.partials.header')
<body>
    @yield('content')
</body>
<footer>
    @include('layouts.partials.footer')
</footer>
</html>
