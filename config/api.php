<?php

return [
    'host' => 'https://kong-staging-platform.dev.cloud.aviata.team',
    'direction_meta_endpoint' => '/airflow/search',
    'direction_info_endpoint' => '/airflow/search/results/',
    'access_token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJhZGE1ZWVkYi1mMDg3LTQzMjQtYmY3Ny0yZjcxMWNkOTY1M2UiLCJpc3MiOiJZdmQ1TFNVbUM3MHVWQnJxNmFFTlFoNjBIQm4waUN6WiIsImlhdCI6MTU0NzgwODE4MCwiZXhwIjoyODcyMzIwMTgwLCJjb25zdW1lciI6eyJpZCI6IjFhMGQ3ZGMyLTZlN2YtNDE5Mi1hY2Y3LTU0NGYyNzQzMzdmYSIsIm5hbWUiOiJjaG9jby1rei5zaXRlIn19.eEM266jdJ2Cc8IEDDkC9lDJbkc3_YBwLpLEkqshcjRg',
    'key' => '1000E',
];
