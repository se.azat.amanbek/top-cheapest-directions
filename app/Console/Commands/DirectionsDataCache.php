<?php

namespace App\Console\Commands;

use App\Repositories\DirectionRepository;
use Illuminate\Console\Command;
use App\Helpers\HttpHelper;
use Illuminate\Support\Facades\Cache;

class DirectionsDataCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'directions:cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Directions info cached successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    const DAY = 86400;
    const MONTH = 2629743;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Cache::flush();
        $httpHelper = HttpHelper::getInstance();
        $apiGetDirectionMetaEndpoint = config('api.host') . config('api.direction_meta_endpoint');
        $apiGetDirectionInfoEndpoint = config('api.host') . config('api.direction_info_endpoint');
        $headers = [
            'Authorization' => 'Bearer ' . config('api.access_token'),
        ];
        $key = config('api.key');

        $directions = DirectionRepository::getPopularityDirections();

        $currentTime = time();

        $daysInCurrentMonth = date('t');
        $currentDay = date('j');
        $requestCount = $daysInCurrentMonth + $currentDay;

        foreach ($directions as $direction) {
            $currentVal = $currentTime;
            $receivedStatus = '';

            for ($i = $currentDay; $i <= $requestCount; $i++) {
                $dateString = date('Ymd', $currentVal);
                $query = ['query' => $direction . $dateString . $key];
                $directionMeta = $httpHelper->post($apiGetDirectionMetaEndpoint, $query, $headers, 'raw');

                if (!isset($directionMeta->id) || !$directionId = $directionMeta->id) {
                    continue;
                }

                $receivedStatus = '';

                while($receivedStatus != 'done') {
                    $directionData = $httpHelper->get($apiGetDirectionInfoEndpoint .  $directionId, $headers);
                    $receivedStatus = isset($directionData['status']) ? $directionData['status'] : '';
                }

                if (count($directionData['items'])) {
                    $firstKey = key($directionData['items']);
                    $minCost = (int) $directionData['items'][$firstKey]['price']['amount'];
                    $minCostTicket = [];

                    foreach ($directionData['items'] as $item) {
                        if ($minCost >= (int) $item['price']['amount']) {
                            $minCost = (int) $item['price']['amount'];
                            $minCostTicket = $item;
                        }
                    }

                    $redisKey = $dateString . ':' . $direction;

                    Cache::forever($redisKey, json_encode($minCostTicket));
                }

                $currentVal = $currentVal + self::DAY;
            }
        }
    }
}
