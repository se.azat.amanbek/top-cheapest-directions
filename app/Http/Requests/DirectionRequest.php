<?php

namespace App\Http\Requests;

use App\Repositories\DirectionRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DirectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'date' => 'required|after_or_equal:today',
        ];

        $allowedDirections = DirectionRepository::getPopularityDirections();

        $rules['direction'] = ['required', Rule::in($allowedDirections)];

        return $rules;
    }
}
