<?php

namespace App\Http\Controllers;

use App\Http\Requests\DirectionRequest;
use App\Repositories\DirectionRepository;

class DirectionController extends Controller
{
    private $directionRepository;

    public function __construct(DirectionRepository $directionRepository)
    {
        $this->directionRepository = $directionRepository;
    }

    public function index()
    {
        $popularDirections = DirectionRepository::getPopularityDirections();
        return view('directions.info', ['popularDirections' => $popularDirections]);
    }

    public function showCheapestTicketOnGivenDate(DirectionRequest $request)
    {
        $result = null;

        $direction = $request->input('direction');
        $date = $request->input('date');
        $formattedDate = date('Ymd', strtotime($date));

        $request = $formattedDate . ':' . $direction;

        if ($directionData = $this->directionRepository->get($request)) {
            $result = json_decode($directionData);
        } else {
            $result = json_decode($directionData);
        }

        return view('directions.show', ['directionData' => $result, 'direction' => $direction]);
    }

}
