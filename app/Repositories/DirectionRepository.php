<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Cache;

class DirectionRepository
{
    const ALMATY = 'ALA';
    const ASTANA = 'TSE';
    const MOSCOW = 'MOW';
    const SAINT_PETERSBURG = 'LED';
    const SHYMKENT = 'CIT';

    public static function getPopularityDirections()
    {
        return [
            self::ALMATY . '-' . self::ASTANA,
            self::ASTANA . '-' . self::SHYMKENT,
            self::ALMATY . '-' . self::MOSCOW,
            self::MOSCOW . '-' . self::ALMATY,
            self::ALMATY . '-' . self::SHYMKENT,
            self::SHYMKENT . '-' . self::ALMATY,
            self::ASTANA . '-' . self::MOSCOW,
            self::MOSCOW . '-' . self::ASTANA,
            self::ASTANA . '-' . self::SAINT_PETERSBURG,
            self::SAINT_PETERSBURG . '-' . self::ASTANA,
        ];
    }

    public function get($key)
    {
        $result = null;

        if (Cache::has($key)) {
            $result = Cache::get($key);
        }

        return $result;
    }
}
