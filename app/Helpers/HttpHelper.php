<?php

namespace App\Helpers;

use GuzzleHttp\Client as GuzzleHttp;
use Illuminate\Http\Response;

/**
 * Class HttpHelper
 * @package App\Helpers
 */

class HttpHelper
{
    /**
     * $client GuzzleHttp\Client instance of guzzle client
     * $defaultHeaders
     * $instance HttpHelper
     */

    private $client;
    private $defaultHeaders;
    private static $instance;

    public function __construct()
    {
        $this->client = new GuzzleHttp();
        $this->defaultHeaders = [
            'Content-Type' => 'application/json',
        ];
    }

    /**
     * This method calls statically and returns a instance of HttpHelper.
     *
     * @return HttpHelper
     */

    public static function getInstance(): HttpHelper
    {
        static::$instance = static::$instance ?? new static();

        return static::$instance;
    }

    /**
     * @param       $endpoint
     * @param array $data
     * @param array $headers
     * @param       $type
     *
     * @return mixed
     */
    public function post($endpoint, array $data = [], array $headers = [], $type = 'url-encoded')
    {
        $headers = array_merge($this->defaultHeaders, $headers);

        $optionsBodyKey = '';

        if ($type == 'url-encoded') {
            $optionsBodyKey = 'form_params';
        } elseif($type == 'raw') {
            $optionsBodyKey = 'json';
        }

        $options = array_merge(['headers' => $headers,], [$optionsBodyKey => $data]);

        try {
            $response = $this->client->post($this->cleanEndpoint($endpoint), $options);

            if (!in_array($response->getStatusCode(), [Response::HTTP_OK, Response::HTTP_CREATED])) {
                return false;
            }

            return json_decode($response->getBody());
        } catch (\Exception $e) {
            //TODO: make logging
        }

        return false;
    }

    /**
     * @param       $endpoint
     * @param array $headers
     *
     * @return mixed
     */
    public function get($endpoint, array  $headers = [])
    {
        $headers = array_merge($this->defaultHeaders, $headers);

        try {
            $response = $this->client->get($this->cleanEndpoint($endpoint), [
                'headers' => $headers,
                'timeout' => 3.14,
            ]);

            return json_decode($response->getBody(), true);
        } catch (\Exception $e) {
            //TODO: make logging
        }

        return false;
    }

    /**
     * Remove leading or trailing forward slashes from the endpoint
     * @param $endpoint
     * @return string
     */
    private function cleanEndpoint($endpoint)
    {
        $endpoint = ltrim($endpoint, "/");
        $endpoint = rtrim($endpoint, "/");

        return $endpoint;
    }
}
